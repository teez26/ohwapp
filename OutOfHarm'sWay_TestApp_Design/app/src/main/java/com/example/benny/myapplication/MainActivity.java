package com.example.benny.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;

import com.example.benny.myapplication.config.Constants;
import com.example.benny.myapplication.config.Message;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapquest.mapping.maps.OnMapReadyCallback;
import com.mapquest.mapping.maps.MapboxMap;

import java.util.concurrent.TimeUnit;

import com.mapquest.mapping.maps.MapView;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import android.os.CountDownTimer;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Camera camera;
    private Camera.Parameters parameters;
    private MapboxMap mMapboxMap;
    private MapView mMapView;
    //String messageToSend = "Lakad Matatag.";
    //String number = "0466140264";
    private final LatLng Monash_Cau = new LatLng(-37.8770054, 145.0420786);

    boolean isFlashOn;
    boolean isTimerRunning = false;
    TextView timerText;
    CountDownTimer countDownTimer;

    int seconds , minutes;
    private static final String FORMAT = "%02d:%02d:%02d";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MapboxAccountManager.start(getApplicationContext());
        setContentView(R.layout.activity_main);

        mMapView = findViewById(R.id.mapquestMapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mMapboxMap = mapboxMap;
                mMapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Monash_Cau,11));
                addMarker(mMapboxMap);
            }
        });
        /**
         * sets initial text for the timer buttons
         */
        final Button sendSoS15 = findViewById(R.id.SOSTimerBtn2);
        sendSoS15.setTag(1);
        sendSoS15.setText("15 Minutes");

        final Button sendSoS = findViewById(R.id.SOSTimerBtn);
        sendSoS.setTag(1);
        sendSoS.setText("5 Minutes");

        final Button sendSoS30 = findViewById(R.id.SOSTimerBtn3);
        sendSoS30.setTag(1);
        sendSoS30.setText("30 Minutes");

        final Button stopTimer = findViewById(R.id.StopTimerBtn);










        Button flashLightButton = findViewById(R.id.flashlightBtn);

        flashLightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlinkFlash();
            }
        });



        Button sendTextBtn = findViewById(R.id.sendTextBtn);
        sendTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSOSText();
                //Intent startIntent = new Intent(getApplicationContext(), SaveDataActivity.class);
                //startActivity(startIntent);

            }
        });


        Button sosSettingsBtn = findViewById(R.id.settingsBtn);
        sosSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), SaveDataActivityFromSettings.class);
                startActivity(startIntent);
            }
        });

        Button mapBtn = findViewById(R.id.map);
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });

        Button tutorialBtn = findViewById(R.id.tutorialBtn);
        tutorialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tute1Intent = new Intent(getApplicationContext(), TutorialActivity.class);
                startActivity(tute1Intent);
            }
        });


        /**
         * sets SOS Timer for 5 minutes with option to reset.
         */

        sendSoS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final int status =(Integer) v.getTag();
                if(status == 1)
                {
                    setTimer(5);
                    sendSoS15.setText("15 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS.setText("Reset");
                    v.setTag(1);
                }
                else
                {
                    sendSoS.setText("5 Minutes");
                    countDownTimer.cancel();
                    setTimer(5);
                    v.setTag(0);
                }
            }
        });

        /**
         * sets SOS Timer for 30 minutes with option to reset.
         */

        sendSoS30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final int status =(Integer) v.getTag();
                if(status == 1)
                {
                    setTimer(30);
                    sendSoS.setText("5 Minutes");
                    sendSoS15.setText("15 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS30.setText("Reset");
                    v.setTag(1);
                }
                else
                {
                    countDownTimer.cancel();
                    setTimer(30);
                    v.setTag(0);
                }
            }
        });


        /*final Button sendSoS15 = findViewById(R.id.SOSTimerBtn2);
        sendSoS15.setTag(1);
        sendSoS15.setText("15 Minutes");*/
        sendSoS15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final int status =(Integer) v.getTag();
                if(status == 1)
                {
                    setTimer(15);
                    sendSoS.setText("5 Minutes");
                    sendSoS30.setText("30 Minutes");
                    stopTimer.setVisibility(v.VISIBLE);
                    sendSoS15.setText("Reset");
                    v.setTag(1);
                }
                else
                {
                    sendSoS15.setText("15 Minutes");
                    countDownTimer.cancel();
                    setTimer(15);
                    v.setTag(0);
                }
            }
        });

        stopTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                stopTimer.setVisibility(v.INVISIBLE);
                sendSoS.setText("5 Minutes");
                sendSoS15.setText("15 Minutes");
                sendSoS30.setText("30 Minutes");
                timerText.setText("00:00");
            }
        });



    }



    /**
     * sets timer
     *
     * @param minutes duration of timer
     */
    public void setTimer(int minutes)
    {
        timerText = (TextView) findViewById(R.id.textView2);
        //boolean isRunning = false;
        if(isTimerRunning){
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(minutes *60 *1000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

                timerText.setText("" + String.format(FORMAT,
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                ////isTimerRunning = true
            }

            public void onFinish() {
                sendSOSText();
                isTimerRunning = false;
                timerText.setText("00:00");
                Toast toast = Toast.makeText(getApplicationContext(), Message.SMS_SENT.getMessage(),Toast.LENGTH_LONG);
                toast.show();
                //timerText.setText(Message.SMS_SENT.getMessage());
            }
        }.start();
        isTimerRunning = true;

    }


    /**
     * Function to send text with a text message.
     */
    public void sendSOSText()
    {
        String phoneNumber, messageToSend;
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
        phoneNumber = preferences.getString(Constants.EMERGENCY_PHONE_NO_SPK,Message.DEFAULT_PREFERENCE_MESSAGE.getMessage());
        messageToSend = preferences.getString(Constants.EMERGENCY_MESSAGE_SPK,Message.DEFAULT_PREFERENCE_MESSAGE.getMessage());
        //TODO check if the information is found in the preferences. If not then prompt him to save the data
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, messageToSend, null,null);
        Toast toast = Toast.makeText(getApplicationContext(),
                Message.SMS_SENT.getMessage(),
                Toast.LENGTH_SHORT);

        toast.show();
    }


    private void addMarker(MapboxMap mapboxMap) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(Monash_Cau);
        markerOptions.title("Monash Caulfield Campus");
        markerOptions.snippet("Welcome to Monash!");
        mapboxMap.addMarker(markerOptions);
    }

    @Override
    public void onResume()
    { super.onResume(); mMapView.onResume(); }

    @Override
    public void onPause()
    { super.onPause(); mMapView.onPause(); }

    @Override
    protected void onDestroy()
    { super.onDestroy(); mMapView.onDestroy(); }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    private void BlinkFlash(){
        String myString = "010101010101010101010101010101010101010111111111111110011111111100111111111001111111111010101010101010101010101";
        long blinkDelay =50; //Delay in ms


        try {
            camera = android.hardware.Camera.open();
        }catch (RuntimeException ex){}

        for (int i = 0; i < myString.length(); i++) {
            if (myString.charAt(i) == '0') {
                parameters = camera.getParameters();
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(parameters);
                camera.startPreview();
                isFlashOn = true;
            } else {
                parameters = camera.getParameters();
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(parameters);
                camera.stopPreview();
                isFlashOn = false;

            }
            try {
                Thread.sleep(blinkDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }

}
