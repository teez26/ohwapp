package com.example.benny.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MessageSentSuccessfulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_sent_successful);
    }
}
