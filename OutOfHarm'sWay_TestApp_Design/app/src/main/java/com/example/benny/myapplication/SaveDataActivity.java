package com.example.benny.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import static android.content.SharedPreferences.*;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.benny.myapplication.config.Constants;
import com.example.benny.myapplication.config.Message;

public class SaveDataActivity extends AppCompatActivity {


    private String numberSelected = null;
    private Button contactButton;
    private EditText editTextMessage;
    private Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_data);

        contactButton = (Button)findViewById(R.id.contactButton);
        editTextMessage = findViewById(R.id.editTextMessage);
        submitButton = findViewById(R.id.submit);

        contactButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, Constants.PICK_CONTACT_REQUEST_CODE);
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(numberSelected!=null){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
                    Editor editor = pref.edit();
                    editor.putString(Constants.EMERGENCY_PHONE_NO_SPK, numberSelected);
                    editor.putString(Constants.EMERGENCY_MESSAGE_SPK, editTextMessage.getText().toString());
                    editor.commit();
                    Intent startIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(startIntent);
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(),
                            Message.CONTACT_UNSELECTED.getMessage(),
                            Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data){
        super.onActivityResult(reqCode, resultCode, data);
        switch(reqCode)
        {
            case (Constants.PICK_CONTACT_REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst())
                    {
                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1"))
                        {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                            phones.moveToFirst();
                            numberSelected = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            // Toast.makeText(getApplicationContext(), cNumber, Toast.LENGTH_SHORT).show();
                            // setCn(cNumber);
                        }
                    }
                }
        }

    }













}
