package com.example.benny.myapplication.config;

public class Constants {
    public static int SPLASH_DURATION = 500;
    public static String FIRST_TIME_SPK = "first_time";
    public static String SHARED_PREFERENCE_NAME = "MyPreference";
    public static String EMERGENCY_PHONE_NO_SPK = "emergency_phone_no";
    public static String EMERGENCY_MESSAGE_SPK = "emergency_message";

    public static final int PICK_CONTACT_REQUEST_CODE = 1;
}
