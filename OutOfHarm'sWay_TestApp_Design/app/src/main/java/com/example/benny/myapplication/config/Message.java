package com.example.benny.myapplication.config;


public enum Message {
    SMS_SENT("Emergency Message Sent"),
    CONTACT_UNSELECTED("No Contact selected. Please select a contact to proceed"),
    DEFAULT_PREFERENCE_MESSAGE("EMPTY STRING");

    private String message;


    Message(String message){
        this.message= message;
    }
    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }
}
