package com.example.benny.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import static android.content.SharedPreferences.*;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.benny.myapplication.config.Constants;

public class homeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int layoutResId = -1;
        final Class targetActivity[] = new Class[1];
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0); // 0 - for private mode
        Editor editor = pref.edit();
        if(pref.getBoolean(Constants.FIRST_TIME_SPK,true)){
            layoutResId = R.layout.activity_save_data;
            targetActivity[0] = SaveDataActivity.class;
            editor.putBoolean(Constants.FIRST_TIME_SPK, false);
            editor.commit();
        }
        else{
            layoutResId = R.layout.activity_home;
            targetActivity[0] = MainActivity.class;
        }

        setContentView(layoutResId);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent homeIntent = new Intent(homeActivity.this, targetActivity[0]);
                startActivity(homeIntent);
                finish();
            }
        }, Constants.SPLASH_DURATION);
    }
}
